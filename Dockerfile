FROM node:latest

ENV ENV prod

WORKDIR /app/back

COPY . /app
RUN cd /app/back && npm install --unsafe-perm=true --allow-root
RUN cd /app && npm install --unsafe-perm=true --allow-root && npm run build
RUN cd /app/back && mkdir views/ && mv ./public/community-job/index.html ./views/

EXPOSE 4000

CMD [ "npm", "run", "start:prod" ]

# docker build -t community-job .
# docker run --rm -p 127.0.0.1:6000:4000 --network server-package_databases -d community-job
