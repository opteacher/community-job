const path = require('path')
const router = require('koa-router')()
const jwt = require('jsonwebtoken')
const { v4: uuidv4 } = require('uuid')

const tools = require('../../../../../utils/tools')
const db = tools.getDatabase()
const {buyTicket} = require(path.join(tools.rootPath(), 'services/trade'))
const {Order} = require(path.join(tools.rootPath(), 'models'))
const svrCfg = tools.readConfig(path.resolve(tools.rootPath(), 'configs/server'), true)

router.post('/', async ctx => {
  ctx.body = {
    result: await buyTicket(ctx.request.body)
  }
})

router.get('/:oid/genToken', async ctx => {
  const order = (await db.select(Order, {id: ctx.params.oid}, {ext: true}))[0]
  const payload = {
    sub: 'useTicket',
    aud: [order.user._id, order.ticket._id, order.number].join('.'),
    iat: Date.now(),
    jti: uuidv4(),
    iss: svrCfg.admin,
    exp: Date.now() + (60 * 60 * 1000) // 1 hour
  }
  ctx.body = {
    result: Object.assign({
      token: jwt.sign(payload, svrCfg.secret)
    }, order.toObject())
  }
})

module.exports = router