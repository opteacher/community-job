const router = require('koa-router')()
const path = require('path')

const rootPath = require('../../../../../utils/tools').rootPath()
const User = require(path.resolve(rootPath, 'models/user'))
const {login, logstat, regup} = require(path.resolve(rootPath, 'services/user'))

router.post('/log/in', ctx => login(ctx, User, 'phone'))

router.get('/log/stat', ctx => logstat(ctx))

router.post('/reg/up', ctx => regup(ctx, User, 'phone'))

module.exports = router