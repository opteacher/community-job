const path = require('path')
const router = require('koa-router')()

const tools = require('../../../../../utils/tools')
const db = tools.getDatabase()
const {Ticket} = require(path.join(tools.rootPath(), 'models'))

router.post('/:tid/message', async ctx => {
  ctx.body = {
    result: await db.save(Ticket,
      {messages: ctx.request.body},
      {id: ctx.params.tid},
      {updMode: 'append'}
    )
  }
})

router.post('/:tid/message/:mid/reply', async ctx => {
  const idCond = {id: ctx.params.tid}
  const ticket = (await db.select(Ticket, idCond))[0].toObject()
  const message = ticket.messages.find(msg => msg._id == ctx.params.mid)
  message.reply = ctx.request.body.reply
  ctx.body = {
    result: await db.save(Ticket, {messages: ticket.messages}, idCond)
  }
})

module.exports = router