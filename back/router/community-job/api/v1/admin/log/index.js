const router = require('koa-router')()
const path = require('path')

const rootPath = require('../../../../../../utils/tools').rootPath()
const Admin = require(path.resolve(rootPath, 'models/admin'))
const {login, logstat} = require(path.resolve(rootPath, 'services/user'))

router.post('/in', ctx => login(ctx, Admin, 'username'))

router.get('/stat', ctx => logstat(ctx))

module.exports = router