const fs = require('fs')
const path = require('path')
const router = require('koa-router')()

const tools = require('../../../../../utils/tools')

router.post('/', async ctx => {
  const key = ctx.request.body.key || path.parse(ctx.request.files.file.path).base
  ctx.body = {
    result: await tools.uploadToQiniu(key,
      fs.createReadStream(ctx.request.files.file.path)
    )
  }
})

module.exports = router