const path = require('path')
const router = require('koa-router')()

const tools = require('../../../../../utils/tools')
const db = tools.getDatabase()
const {Result, Job} = require(path.join(tools.rootPath(), 'models'))
const {updUserExp} = require(path.join(tools.rootPath(), 'services/user'))

router.post('/:jid/result', async ctx => {
  const reqBody = ctx.request.body
  const result = await db.save(Result, Object.assign({job: ctx.params.jid}, reqBody))
  await db.save(Job, {proccess: 1}, {id: ctx.params.jid}, {updMode: 'append'})
  await updUserExp(reqBody.user, '提交结果')
  ctx.body = {result}
})

module.exports = router