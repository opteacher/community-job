const _ = require('lodash')
const fs = require('fs')
const path = require('path')
const router = require('koa-router')()

const tools = require('../../../../../../utils/tools')
const db = tools.getDatabase()
const {Result, User} = require(path.join(tools.rootPath(), 'models'))
const {getActScore, updUserExp} = require(path.join(tools.rootPath(), 'services/user'))
const {chkJobDone} = require(path.join(tools.rootPath(), 'services/jobs'))

router.post('/image', async ctx => {
  ctx.body = {
    result: await tools.uploadToQiniu(ctx.request.body.key,
      fs.createReadStream(ctx.request.files.avatar.path)
    )
  }
})

router.put('/:rid/judge', async ctx => {
  const reqBody = ctx.request.body
  const score = await getActScore(reqBody.uid, '判断结果')
  const result = await db.save(Result, {
    judges: {
      uid: reqBody.uid,
      judge: reqBody.reality ? 1 : -1,
      createdAt: new Date()
    },
    score: reqBody.reality ? score : -score
  }, {id: ctx.params.rid}, {updMode: 'append'})
  await chkJobDone(ctx.params.rid)
  await updUserExp(reqBody.uid, '认证结果')
  ctx.body = {result}
})

router.post('/:rid/comment', async ctx => {
  const reqBody = ctx.request.body
  const cmtUser = (await db.select(User, {id: reqBody.uid}))[0]
  let result = (await db.select(Result, {id: ctx.params.rid}))[0]
  const orgCmts = result.comments
  result = await db.save(Result, {
    comments: {
      autId: cmtUser._id,
      author: cmtUser.username || cmtUser.phone,
      avatar: cmtUser.avatar || 'holder.js/32x32?text=A',
      content: reqBody.content,
      likes: [],
      dislikes: [],
      createdAt: new Date()
    }
  }, {id: ctx.params.rid}, {updMode: 'append'})
  // 只给第一次评论的用户加经验
  if (!orgCmts.find(cmt => cmt.autId === reqBody.uid)) {
    await updUserExp(reqBody.uid, '发布评论')
  }
  ctx.body = {result}
})

router.put('/:rid/comment/:cid/like-or-not', async ctx => {
  const reqBody = ctx.request.body
  const idCond = {id: ctx.params.rid}
  const result = (await db.select(Result, idCond))[0].toObject()
  const comment = result.comments.find(cmt => cmt._id == ctx.params.cid)
  const lkOrNt = reqBody.like ? 'likes' : 'dislikes'
  if (comment[lkOrNt].includes(reqBody.uid)) {
    _.pull(comment[lkOrNt], reqBody.uid)
  } else {
    comment[lkOrNt].push(reqBody.uid)
  }
  ctx.body = {
    result: await db.save(Result, {
      comments: result.comments
    }, idCond)
  }
})

module.exports = router