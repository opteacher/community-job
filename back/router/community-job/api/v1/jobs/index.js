const _ = require('lodash')
const fs = require('fs')
const path = require('path')
const router = require('koa-router')()

const tools = require('../../../../../utils/tools')
const db = tools.getDatabase()
const {Job, Result} = require(path.join(tools.rootPath(), 'models'))

router.get('/', async ctx => {
  const result = []
  const today = new Date()
  for (const record of await db.select(Job, {})) {
    const job = record.toObject()
    if (job.proccess && job.proccess === job.number // 进度监测
    || (today < job.duration[0] || today > job.duration[1]) // 时间监测
    ) {
      continue
    }
    // 条件监测
    if ((await db.select(Result, {'user': ctx.query.uid})).length) {
      result.push(Object.assign({status: '已提交'}, job))
    } else {
      result.push(Object.assign({status: '可接'}, job))
    }
  }
  ctx.body = {result}
})

module.exports = router