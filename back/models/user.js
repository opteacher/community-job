const db = require('../databases/mongo')
const { createHmac } = require('crypto')
const { getSecret } = require('../utils/tools')

module.exports = db.defineModel({
  __modelName: 'user',
  username: db.Types.String,
  password: db.Types.String,
  phone: db.Types.String,
  realname: db.Types.String,
  idCard: db.Types.String,
  address: db.Types.String,
  company: db.Types.String,
  wkAddress: db.Types.String,
  level: db.Types.Number,
  experience: db.Types.Number,
  inputs: [{ type: db.Types.Id, ref: 'transaction' }],
  outputs: [{ type: db.Types.Id, ref: 'transaction' }],
  tickets: [{ type: db.Types.Id, ref: 'order' }]
}, {
  middle: {
    create: {
      before (doc) {
        if (doc.password.length !== 64) {
          doc.password = createHmac('sha256', getSecret())
            .update(doc.password).digest('hex')
        }
      }
    }
  },
  router: {
    methods: ['GET', 'ALL', 'PUT']
  }
})
