const db = require('../databases/mongo')

module.exports = db.defineModel({
  __modelName: 'job',
  name: db.Types.String,
  intro: db.Types.String,
  desc: db.Types.String,
  rewards: db.Types.Number,
  qrCode: db.Types.String,
  proccess: db.Types.Number,
  number: db.Types.Number,
  numPerX: db.Types.Array,
  duration: [db.Types.Date],
  repeatable: db.Types.Boolean,
  totalJudges: db.Types.Number,
  doneScore: db.Types.Number,
  qualities: db.Types.Array,
  results: db.Types.Array,
}, {
  router: {
    methods: ['GET', 'ALL', 'POST', 'DELETE', 'PUT']
  }
})
