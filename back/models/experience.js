const db = require('../databases/mongo')

module.exports = db.defineModel({
  __modelName: 'experience',
  level: db.Types.Number,
  '发布任务': db.Types.Number,
  '提交结果': db.Types.Number,
  '完成任务': db.Types.Number,
  '发布评论': db.Types.Number,
  '认证结果': db.Types.Number,
  '点赞评论': db.Types.Number,
}, {
  router: {
    methods: ['GET', 'ALL']
  }
})
