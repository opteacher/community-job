const db = require('../databases/mongo')

module.exports = db.defineModel({
  __modelName: 'ticket',
  name: db.Types.String,
  cover: db.Types.String,
  price: db.Types.Number,
  number: db.Types.Number,
  left: db.Types.Number,
  desc: db.Types.String,
  preferences: db.Types.Array,
  messages: [db.defineModel({
    __modelName: 'message',
    aid: { type: db.Types.Id, ref: 'user' },
    author: db.Types.String,
    content: db.Types.String,
    reply: db.Types.String,
    createdAt: db.Types.Date,
    repliedAt: db.Types.Date,
  }, {
    middle: {
      create: {
        before (doc) {
          if (!doc.createdAt) {
            doc.createdAt = Date.now()
          }
          doc.repliedAt = Date.now()
        }
      }
    }
  }).schema]
}, {
  router: {
    methods: ['GET', 'ALL', 'POST', 'DELETE', 'PUT']
  }
})
