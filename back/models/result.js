const db = require('../databases/mongo')

module.exports = db.defineModel({
  __modelName: 'result',
  user: { type: db.Types.Id, ref: 'user' },
  job: { type: db.Types.Id, ref: 'job' },
  results: [db.Types.String],
  comments: [db.defineModel({
    __modelName: 'comment',
    autId: db.Types.Id,
    author: db.Types.String,
    avatar: db.Types.String,
    content: db.Types.String,
    likes: [db.Types.String],
    dislikes: [db.Types.String],
    createdAt: db.Types.Date
  }).schema],
  status: db.Types.String, // 评分中/申诉中/已失败/已完成
  judges: [db.defineModel({
    __modelName: 'judge',
    uid: db.Types.Id,
    judge: db.Types.Number, // 1：真；-1：假
    createdAt: db.Types.Date
  }).schema],
  score: db.Types.Number,
  createdAt: db.Types.Date
}, {
  middle: {
    create: {
      before (doc) {
        if (!doc.createdAt) {
          doc.createdAt = Date.now()
        }
      }
    }
  },
  router: {
    methods: ['GET', 'ALL', 'POST', 'PUT']
  }
})
