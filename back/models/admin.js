const db = require('../databases/mongo')
const { createHmac } = require('crypto')
const { getSecret } = require('../utils/tools')

module.exports = db.defineModel({
  __modelName: 'admin',
  username: db.Types.String,
  password: db.Types.String,
}, {
  middle: {
    create: {
      before (doc) {
        if (doc.password.length !== 64) {
          doc.password = createHmac('sha256', getSecret()).update(doc.password).digest('hex')
        }
      }
    }
  },
  router: {
    methods: ['GET', 'ALL', 'POST']
  }
})
