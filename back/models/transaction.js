const db = require('../databases/mongo')

module.exports = db.defineModel({
  __modelName: 'transaction',
  from: db.Types.Id,
  source: db.Types.String, // 奖励/购买/转账
  to: { type: db.Types.Id, ref: 'user' }, 
  amount: db.Types.Number,
  time: db.Types.Date
}, {
  middle: {
    create: {
      before (doc) {
        if (!doc.time) {
          doc.time = Date.now()
        }
      }
    }
  },
  router: {
    methods: ['GET', 'ALL', 'POST']
  }
})