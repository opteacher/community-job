const db = require('../databases/mongo')

module.exports = db.defineModel({
  __modelName: 'score',
  level: db.Types.Number,
  '完成任务': db.Types.Number,
  '发布评论': db.Types.Number,
  '判断结果': db.Types.Number,
}, {
  router: {
    methods: ['GET', 'ALL']
  }
})
