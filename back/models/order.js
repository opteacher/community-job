const db = require('../databases/mongo')

module.exports = db.defineModel({
  __modelName: 'order',
  ticket: { type: db.Types.Id, ref: 'ticket' },
  user: { type: db.Types.Id, ref: 'user' },
  number: db.Types.Number,
  cost: db.Types.Number,
  remark: db.Types.String,
  status: db.Types.String, // 未使用/已使用/已失效
  usedAt: db.Types.Date,
  createdAt: db.Types.Date
}, {
  middle: {
    create: {
      before (doc) {
        if (!doc.createdAt) {
          doc.createdAt = Date.now()
        }
      }
    }
  },
  router: {
    methods: ['GET', 'ALL', 'DELETE']
  }
})
