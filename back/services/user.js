const _ = require('lodash')
const path = require('path')
const crypto = require('crypto')
const jwt = require('jsonwebtoken')
const { v4: uuidv4 } = require('uuid')

const tools = require('../utils/tools')
const db = tools.getDatabase()
const svrCfg = tools.readConfig(path.resolve(tools.rootPath(), 'configs/server'), true)
const {Score, User, Experience} = require(path.join(tools.rootPath(), 'models'))

const exp = {
  async login (ctx, model, key) {
    const reqBody = ctx.request.body

    const result = await db.select(model, {
      [key]: reqBody[key]
    })
    if(!result.length) {
      ctx.body = { error: '用户名不存在' }
      return
    }
    const user = result[0]

    const reqPwd = crypto.createHmac('sha256', tools.getSecret())
      .update(reqBody.password).digest('hex')
    if (reqPwd !== user.password) {
      ctx.body = { error: '错误的登录密码' }
      return
    }

    const payload = {
      sub: 'login',
      aud: user._id,
      iat: Date.now(),
      jti: uuidv4(),
      iss: svrCfg.admin,
      exp: Date.now() + (24 * 60 * 60 * 1000) // 1 day
    }
    let logined = user.toObject()
    delete logined.password
    ctx.body = {
      result: {
        logined,
        token: jwt.sign(payload, svrCfg.secret),
        message: '登录成功！'
      }
    }
  },
  async logstat (ctx) {
    const reqQuery = ctx.request.query

    const token = reqQuery.token
    if(!token) {
      ctx.body = { error: '未登录' }
      return
    }

    try {
      jwt.verify(token, svrCfg.secret)
    } catch (e) {
      ctx.body = {
        error: `登录token验证失败：${e.message || JSON.stringify(e)}`
      }
      return
    }

    ctx.body = {
      result: {
        message: '验证通过'
      }
    }
  },
  async regup (ctx, model, key) {
    const reqBody = ctx.request.body

    const stored = await db.select(model, {
      [key]: reqBody[key]
    })
    if (stored.length) {
      ctx.body = {
        error: '用户名已经存在'
      }
      return
    }

    const result = await db.save(model, reqBody)
    if(typeof result === "string") {
      ctx.body = {
        error: `持久化用户数据失败：${result}`
      }
    } else {
      ctx.body = {result}
    }
  },
  async getActScore (uid, action) {
    const user = (await db.select(User, {id: uid}))[0]
    const score = (await db.select(Score, {level: user.level}))[0]
    return score[action]
  },
  async updUserExp (uid, action) {
    const cond = {id: uid}
    const user = (await db.select(User, cond))[0].toObject()
    if (user.level === -1) {
      return Promise.resolve()
    }
    const exp = (await db.select(Experience, {level: user.level}))[0]
    user.experience += exp[action]
    if (user.experience >= 100) {
      user.level++
      user.experience -= 100
    }
    return db.save(User, _.pick(user, ['level', 'experience']), cond)
  },
}

module.exports = exp