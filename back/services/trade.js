const path = require('path')

const tools = require('../utils/tools')
const db = tools.getDatabase()
const {User, Transaction, Order, Ticket} = require(path.join(tools.rootPath(), 'models'))

const exp = {
  async buyTicket (reqBody) {
    const ret = await db.save(Order, reqBody)
    await db.save(Ticket,
      {number: -reqBody.number},
      {id: reqBody.ticket},
      {updMode: 'append'}
    )
    const result = await db.save(Transaction, {
      from: reqBody.user,
      source: '购买',
      amount: reqBody.cost,
    })
    const optTx = result[0]
    await db.save(User,
      {outputs: optTx._id, tickets: ret[0]._id},
      {id: reqBody.user},
      {updMode: 'append'}
    )
    return Promise.resolve(ret)
  }
}

module.exports = exp