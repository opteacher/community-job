const path = require('path')

const tools = require('../utils/tools')
const db = tools.getDatabase()
const {User, Result, Transaction} = require(path.join(tools.rootPath(), 'models'))

const exp = {
  async chkJobDone (rid) {
    const idCond = {id: rid}
    const result = (await db.select(Result, idCond, {ext: true}))[0]
    const fourTimesScore = result.job.doneScore << 2
    if (result.score > result.job.doneScore && result.score > fourTimesScore) {
      // 分数大于四倍真认证，直接判定为真
      return exp.jobDone(result)
    } else if (result.score < result.job.doneScore && result.score < -fourTimesScore) {
      // 分数小于四倍真认证，直接判定为假
      return db.save(Result, {status: '已失败'}, idCond)
    }
    if (result.judges.length > result.job.totalJudges) {
      // 认证数大于任务所需认证数
      if (result.score >= result.job.doneScore) {
        // 分数达标
        return exp.jobDone(result)
      } else {
        return db.save(Result, {status: '已失败'}, idCond)
      }
    }
    return Promise.resolve()
  },
  async jobDone (result) {
    const ret = await db.save(Result,
      {status: '已完成'},
      {id: result._id}
    )
    const iptTx = (await db.save(Transaction, {
      from: result._id,
      source: '奖励',
      to: result.user._id,
      amount: result.job.rewards,
    }))[0]
    await db.save(User,
      {inputs: iptTx._id},
      {id: result.user._id},
      {updMode: 'append'}
    )
    return Promise.resolve(ret)
  },
}

module.exports = exp