import Vue from 'vue'
import Antd from 'ant-design-vue'
import 'ant-design-vue/dist/antd.css'
import './commons/styles.css'
import App from './App.vue'
import router from './router'
import store from './store'
import VueHolder from 'vue-holderjs'

Vue.use(VueHolder)
Vue.config.productionTip = false
Vue.use(Antd)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
