import Vue from 'vue'
import Vuex from 'vuex'
import utils from '../commons/utils'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    authTkn: '',
    lgnUser: null
  },
  mutations: {
    SET_AUTH_TKN (state, token) {
      state.authTkn = token
    },
    SET_LGN_USR (state, user) {
      state.lgnUser = user
      state.lgnUser.account = state.lgnUser.username || state.lgnUser.phone
    }
  },
  actions: {
    async REFRESH_LGN_USER (ctx) {
      const path = `/community-job/mdl/v1/user/${ctx.state.lgnUser._id}`
      const user = (await utils.reqBack(null, path, 'get'))[0]
      delete user.password
      user.balance = utils.userBalance(user)
      console.log(user)
      ctx.commit('SET_LGN_USR', user)
    }
  }
})
