import Vue from 'vue'
import VueRouter from 'vue-router'
import axios from 'axios'
import utils from '../commons/utils'
import store from '../store'
import Home from '../views/Home'
import AdmLogin from '../views/AdmLogin'
import AdmAddJob from '../views/AdmAddJob'
import AdmVwJobs from '../views/AdmVwJobs'
import Jobs from '../views/Jobs'
import JobDetail from '../views/JobDetail'
import News from '../views/News'
import Account from '../views/Account'
import SbtResult from '../views/SbtResult'
import SbtResultScs from '../views/SbtResultScs'
import Login from '../views/Login'
import Shop from '../views/Shop'
import Ticket from '../views/Ticket'
import AdmAddTicket from '../views/AdmAddTicket'
import AdmVwTickets from '../views/AdmVwTickets'
import AdmVwTktMsgs from '../views/AdmVwTktMsgs'
import CfmOrder from '../views/CfmOrder'
import OrderScs from '../views/OrderScs'
import MyJobs from '../views/MyJobs'
import MyAsset from '../views/MyAsset'
import MyTickets from '../views/MyTickets'
import MyTktQrCd from '../views/MyTktQrCd'

Vue.use(VueRouter)

const routes = [
  {
    path: '/community-job/home',
    name: 'Home',
    component: Home
  },
  {
    path: '/community-job/admin/login',
    name: 'AdminLogin',
    component: AdmLogin
  },
  {
    path: '/community-job/admin/addJob',
    name: 'AdminAddJob',
    component: AdmAddJob,
    meta: {reqAdmin: true}
  },
  {
    path: '/community-job/admin/addTicket',
    name: 'AdminAddTicket',
    component: AdmAddTicket,
    meta: {reqAdmin: true}
  },
  {
    path: '/community-job/admin/vwTickets',
    name: 'AdminViewTickets',
    component: AdmVwTickets,
    meta: {reqAdmin: true}
  },
  {
    path: '/community-job/admin/vwTktMsgs',
    name: 'AdminViewTicketMessages',
    component: AdmVwTktMsgs,
    meta: {reqAdmin: true}
  },
  {
    path: '/community-job/admin/vwJobs',
    name: 'AdminViewJobs',
    component: AdmVwJobs,
    meta: {reqAdmin: true}
  },
  {
    path: '/community-job/jobs',
    name: 'Jobs',
    component: Jobs,
    meta: {reqLogin: true}
  },
  {
    path: '/community-job/jobDetail',
    name: 'JobDetail',
    component: JobDetail,
    meta: {reqLogin: true}
  },
  {
    path: '/community-job/sbtResult',
    name: 'SbtResult',
    component: SbtResult,
    meta: {reqLogin: true}
  },
  {
    path: '/community-job/sbtResult/success',
    name: 'SbtResultScs',
    component: SbtResultScs,
    meta: {reqLogin: true}
  },
  {
    path: '/community-job/news',
    name: 'News',
    component: News,
    meta: {reqLogin: true}
  },
  {
    path: '/community-job/shop',
    name: 'Shop',
    component: Shop,
    meta: {reqLogin: true}
  },
  {
    path: '/community-job/ticket',
    name: 'Ticket',
    component: Ticket,
    meta: {reqLogin: true}
  },
  {
    path: '/community-job/cfmOrder',
    name: 'ConfirmOrder',
    component: CfmOrder,
    meta: {reqLogin: true}
  },
  {
    path: '/community-job/order/success',
    name: 'OrderSuccess',
    component: OrderScs,
    meta: {reqLogin: true}
  },
  {
    path: '/community-job/account',
    name: 'Account',
    component: Account,
    meta: {reqLogin: true}
  },
  {
    path: '/community-job/account/jobs',
    name: 'MyJobs',
    component: MyJobs,
    meta: {reqLogin: true}
  },
  {
    path: '/community-job/account/asset',
    name: 'MyAsset',
    component: MyAsset,
    meta: {reqLogin: true}
  },
  {
    path: '/community-job/account/tickets',
    name: 'MyTickets',
    component: MyTickets,
    meta: {reqLogin: true}
  },
  {
    path: '/community-job/account/ticket/qrcode',
    name: 'MyTicketQRCode',
    component: MyTktQrCd,
    meta: {reqLogin: true}
  },
  {
    path: '/community-job/login',
    name: 'Login',
    component: Login
  }
]

const router = new VueRouter({
  routes, mode: 'history'
})

async function validLogin (role) {
  const path = `/community-job/api/v1/${role}/log/stat`
  const resp = await axios.get(utils.bkHost + path, {
    params: { token: store.state.authTkn }
  })
  return Promise.resolve(resp.data.error)
}

router.beforeEach(async (to, _from, next) => {
  if (to.matched.some(record => record.meta.reqAdmin)) {
    const error = await validLogin('admin')
    if (error) {
      next({
        path: '/community-job/admin/login',
        query: { redirect: to.fullPath, error }
      })
    } else {
      next()
    }
  } else if (to.matched.some(record => record.meta.reqLogin)) {
    const error = await validLogin('user')
    if (error) {
      next({
        path: '/community-job/login',
        query: { redirect: to.fullPath, error }
      })
    } else {
      next()
    }
  } else {
    next() // 确保一定要调用 next()
  }
})

export default router
