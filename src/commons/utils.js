const $ = require('jquery')
const axios = require('axios')
const moment = require('moment')

const exp = {
  async waitFor (select, cdFun = null, lpLimit = 500) {
    let ret = []
    for (let i = 0; i < lpLimit; ++i) {
      ret = $(select)
      if (ret.length) {
        if (cdFun && cdFun(ret)) {
          if (cdFun(ret)) {
            return Promise.resolve(ret)
          }
        } else {
          return Promise.resolve(ret)
        }
      }
      await new Promise(res => setTimeout(res, 200))
    }
    return Promise.resolve(ret)
  },
  async until (cdFun, lpLimit = 500) {
    for (let i = 0; i < lpLimit; ++i) {
      if (cdFun()) {
        return Promise.resolve()
      }
      await new Promise(res => setTimeout(res, 200))
    }
    return Promise.reject()
  },
  round (value, n) {
    return Math.round(value * Math.pow(10, n)) / Math.pow(10, n)
  },
  bkHost: process.env.NODE_ENV === 'production' ? '' : 'http://127.0.0.1:4000',
  async reqBack (self, path, method, params = {}, options = {}) {
    const hide = self ? self.$message.loading('加载中……') : () => {}
    const resp = await axios[method](exp.bkHost + path, params, options)
    hide()
    if (!resp.data) {
      self && self.$notification.error({
        message: '返回体没有data字段！'
      })
      return Promise.reject()
    }
    if (!resp.data.data && !resp.data.result) {
      self && self.$notification.error({
        message: JSON.stringify(resp.data)
      })
      return Promise.reject()
    }
    return Promise.resolve(resp.data.data || resp.data.result)
  },
  jobJsonToForm (jsonData, form) {
    form.setFieldsValue({
      name: jsonData.name,
      intro: jsonData.intro,
      desc: jsonData.desc,
      rewards: jsonData.rewards,
      number: jsonData.number,
      perX: jsonData.numPerX[1],
      num: jsonData.numPerX[1] !== 'none' ? jsonData.numPerX[0] : undefined,
      qrCode: jsonData.qrCode || '',
      duration: [
        moment(jsonData.duration[0]),
        moment(jsonData.duration[1]),
      ],
      totalJudges: jsonData.totalJudges,
      realsRate: (jsonData.realsRate[0] - 1) * 20,
      qualities: jsonData.qualities || [],
      results: jsonData.results || []
    })
  },
  ticketJsonToForm (jsonData, form) {
    form.setFieldsValue({
      name: jsonData.name,
      cover: jsonData.cover,
      price: jsonData.price,
      number: jsonData.number,
      desc: jsonData.desc,
      preferences: jsonData.preferences || [],
    })
  },
  userBalance (user) {
    const inputs = user.inputs.length ? user.inputs.map(ipt => ipt.amount).reduce((a, c) => a + c) : 0
    const outputs = user.outputs.length ? user.outputs.map(opt => opt.amount).reduce((a, c) => a + c) : 0
    return inputs - outputs
  }
}

module.exports = exp